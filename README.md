# 🚀 Welcome to the World Wide Bank project

This coding challenge has been prepared by Stephen Dixon

To start the server type

```
npm start
```

You will then be able to access the server at 

http://localhost:8000

To log in, use the customer ID as the username and "admin" as your password.

You can sign out by refreshing the page or by clicking on the sign out button.
