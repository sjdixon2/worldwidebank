import React, {useState} from 'react';
import DepositModal from './DepositModal';
import Button from 'react-bootstrap/Button';
import WithdrawalModal from './WithdrawalModal';
import TransferModal from './TransferModal';


export default function ModalLauncher(props){
  var [isDepositModalActive, setDepositModalActive] = useState(false);
  var [isWithdrawalModalActive, setWithdrawalModalActive] = useState(false);
  var [isTransferModalActive, setTransferModalActive] = useState(false);
  
  const hideDepositModal = () => setDepositModalActive(false);
  const hideWithdrawalModal = () => setWithdrawalModalActive(false);
  const hideTransferModal = () => setTransferModalActive(false);
  
  const showDepositModal = () => setDepositModalActive(true);
  const showWithdrawalModal = () => setWithdrawalModalActive(true);
  const showTransferModal = () => setTransferModalActive(true);

  return (
    <>
      <Button  onClick={showDepositModal}>Deposit Money</Button>
      <Button  onClick={showWithdrawalModal}>Withdraw Money</Button>
      <Button  onClick={showTransferModal}>Transfer Money</Button>
      <Button onClick={() => location.reload()}>Sign out</Button>

      <DepositModal show={isDepositModalActive} onHide={hideDepositModal} model={props.model} onModalSuccess={props.onModalSuccess}/>
      <WithdrawalModal show={isWithdrawalModalActive} onHide={hideWithdrawalModal} model={props.model} onModalSuccess={props.onModalSuccess} />
      <TransferModal show={isTransferModalActive} onHide={hideTransferModal} model={props.model} onModalSuccess={props.onModalSuccess} />
    </>
  );
} 