import React, {useState} from 'react';
import Modal from "react-bootstrap/Modal";
import Form from 'react-bootstrap/Form';
import Retriever from '../api/Retriever';
import Button from 'react-bootstrap/Button';
import AccountFormControl from './AccountFormControl';
import TransactionValueFormControl from './TransactionValueFormControl';

export default function TransferModal({model, show, onHide, onModalSuccess}) {
  const [fromAccountId, setFromAccountId] = useState("");
  const [toAccountId, setToAccountId] = useState("");
  const [amount, setAmount] = useState("");
  const [currency, setCurrency] = useState("CAD");
  const [feedback, setFeedback] = useState("");


  function onSubmit(event){
    var transfer = {
      transactionType: "WITHDRAWAL",
      customerId: model.customerId,
      fromAccountId,
      toAccountId,
      value: {
        amount: amount,
        currency: currency
      }
    }
    Retriever.createTransfer(transfer, model.token, function(){
      onModalSuccess();
      onHide();
    }, function(response){
      event.preventDefault();
      setFeedback(response.data);
    });
  }

  return (
    <Modal className="modal" show={show} onHide={onHide} closebutton="true">
        
      <Form onSubmit={onSubmit}>
        <Modal.Header closeButton>
          <Modal.Title>Transfer Money Money</Modal.Title> 
        </Modal.Header>
        <Modal.Body>
          <AccountFormControl key="from" model={model} accountId={fromAccountId} setAccountId={setFromAccountId} label="From"/>
          <AccountFormControl key="to" model={model} accountId={toAccountId} setAccountId={setToAccountId} label="To"/>
          <TransactionValueFormControl model={model} amount={amount} currency={currency} setAmount={setAmount} setCurrency={setCurrency} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onHide}>Cancel</Button>
          <Button variant="primary" type="submit">Transfer Money</Button>
          <p>{feedback}</p>
        </Modal.Footer>
      </Form>

    </Modal>
  );
}