import React from 'react';
import Form from 'react-bootstrap/Form';

export default function TransactionValueFormControl({amount, currency, setAmount, setCurrency}){
  return (
    <>
      <Form.Group controlId="amount">
        <Form.Label>Amount</Form.Label>
        <Form.Control 
          type="number" 
          required
          value={amount} 
          onChange={e => setAmount(e.target.value)} 
        >
        </Form.Control>
      </Form.Group>

      <Form.Group controlId="currency">
        <Form.Label>Currency</Form.Label>
        <Form.Control 
          as="select"
          required
          value={currency} 
          onChange={e => setCurrency(e.target.value)} 
        >
          <option>CAD</option>
          <option>MXN</option>
          <option>USD</option>
        </Form.Control>
      </Form.Group> 
    </>
  );
}