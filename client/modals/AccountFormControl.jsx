import React from 'react';
import Form from 'react-bootstrap/Form';

export default function AccountFormControl({accountId, setAccountId, model, label}){
  const accountIdLabel = label ? label : "Account ID";
  return (
    <Form.Group controlId="accountId">
    <Form.Label>{accountIdLabel}</Form.Label>
    <Form.Control 
      as="select" 
      value={accountId} 
      onChange={e => setAccountId(e.target.value)}
    >
      <option value="0">
        Choose...
      </option>
      {model.accounts.map(account => (<option key={account.accountId} value={account.accountId}>{account.accountId}</option>))}
    </Form.Control>
  </Form.Group>
  );
}