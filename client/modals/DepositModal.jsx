import React, {useState} from 'react';
import Modal from "react-bootstrap/Modal";
import Form from 'react-bootstrap/Form';
import Retriever from '../api/Retriever';
import Button from 'react-bootstrap/Button';
import AccountFormControl from './AccountFormControl';
import TransactionValueFormControl from './TransactionValueFormControl';

export default function DepositModal({model, show, onHide, onModalSuccess}) {
  const [accountId, setAccountId] = useState("");
  const [amount, setAmount] = useState("");
  const [currency, setCurrency] = useState("CAD");
  const [feedback, setFeedback] = useState("");

  function onSubmit(event){
    var deposit = {
      transactionType: "DEPOSIT",
      customerId: model.customerId,
      accountId,
      value: {
        amount: amount,
        currency: currency
      }
    }
    Retriever.createDeposit(deposit, model.token, function(){
      onModalSuccess();
      onHide();
    }, function(response){
      event.preventDefault();
      setFeedback(response.data);
    });
  }

  return (
    <Modal className="modal" show={show} onHide={onHide} closebutton="true">
        
      <Form onSubmit={onSubmit}>
        <Modal.Header closeButton>
          <Modal.Title>Deposit Money</Modal.Title> 
        </Modal.Header>
        <Modal.Body>
          <AccountFormControl model={model} accountId={accountId} setAccountId={setAccountId} />
          <TransactionValueFormControl model={model} amount={amount} currency={currency} setAmount={setAmount} setCurrency={setCurrency} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onHide}>Cancel</Button>
          <Button variant="primary" type="submit">Deposit Money</Button>
          <p>{feedback}</p>
        </Modal.Footer>
      </Form>

    </Modal>
  );
}