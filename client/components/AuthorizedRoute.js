import React, { Component } from 'react';
import {Route, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';


import { setAuthenticationState, setAuthenticationFailure} from  "../reducers/AuthenticationSlice";

const mapStateToProps = state => {
  return {
    customerId: state.authentication.customerId,
    token: state.authentication.token
  };
};
const mapDispatchToProps = { 
  setAuthenticationState, 
  setAuthenticationFailure
};

class AuthorizedRoute extends Component {
  constructor(props){
    super(props);
    this.state = {
      path: props.path,
      component: props.component,
      customerId: props.customerId,
      token: props.token
    };
  }

  render(){
    if (!this.props.token){
      return (<Redirect to="/login" />);
    }
    return (
      <Route path={this.state.path} component={this.state.component} />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorizedRoute);
