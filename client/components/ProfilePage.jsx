import React, {Component} from 'react';
import {connect} from 'react-redux';
import Container from 'react-bootstrap/Container';

import AccountWidget from './AccountWidget';
import Retriever from '../api/Retriever';
import ModalLauncher from '../modals/ModalLauncher';
import { setProfileDetailsSuccess, setProfileDetailsFailure} from '../reducers/ProfileSlice';

import '../common.css';

const mapStateToProps = state => {
  return {
    customerId: state.authentication.customerId,
    name: state.profileDetails.name,
    accounts: state.profileDetails.accounts,
    token: state.authentication.token
  };
};

const mapDispatchToProps = { setProfileDetailsSuccess, setProfileDetailsFailure};

class ProfilePage extends Component {
  constructor(props){
    super(props);
    this.state = {
      customerId:  props.customerId,
      name: props.name,
      accounts: props.accounts,
      token: props.token
    };

    this.refreshState = this.refreshState.bind(this);

  }

  componentDidMount() {
    this.refreshState(this);
  }

  refreshState(){
    var self = this;
    Retriever.getCustomer(this.state.customerId, this.state.token, function(data){
      self.props.setProfileDetailsSuccess(data);
      // currently not sure why, but this is required for data from store to be displayed.
      self.setState(data);

    }, function(error){
      throw error;
    });
  }

  render() {
  
    return (
      <Container className="Profile" role="main">
        <div className="d-flex align-items-center p-3 margin-3 salutation rounded box-shadow">
        <h3>Welcome {this.state.name}!</h3>
        <ModalLauncher model={this.state} onModalSuccess={this.refreshState}/>
        </div>

        {this.state.accounts.map(account => {
          return (<AccountWidget key={account.accountId} model={account} />);
        })}

      </Container>
    );  
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
