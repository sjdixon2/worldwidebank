import React, {useState} from 'react';
import Container from 'react-bootstrap/Container';  
import Table from 'react-bootstrap/Table';
import Money from './Money';
import _ from 'lodash';

import '../common.css';

export default function AccountWidget(props){
  var model = props.model;
  var allTransactions = _.concat(model.debits, model.credits);
  var sortedTransactions = _.sortBy(allTransactions, "transactionID");

  return (
    <Container>
      <div className="margin-3 p-3 rounded box-shadow"> 
        <div className="border-bottom pb-2 mb-0">
          <h3>Account #{model.accountId}</h3>
          <h4>Account Holders:</h4>
          <ul>
            {model.accountHolders.map(accountHolder => (<li key={accountHolder.customerId}>{accountHolder.name}</li>))}
          </ul>
          <h4>Current Balance: <Money value={model.balance} /></h4>
        </div>

        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>Transaction ID</th>
              <th>Customer ID</th>
              <th>Transaction Type</th>
              <th>Transaction Value</th>
            </tr>
          </thead>
          <tbody>
            {sortedTransactions.map(transaction => (
              <tr key={transaction.transactionID}> 
                <td> {transaction.transactionID} </td>
                <td> {transaction.customerId} </td>
                <td> {transaction.transactionType} </td>
                <td> <Money value={transaction.value} /> </td>
              </tr>)
            )}
          </tbody>
        </Table>

      </div>
    </Container>
  )
}