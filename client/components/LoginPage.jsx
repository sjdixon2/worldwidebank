import React, {useState} from 'react';
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom';
import Retriever from "../api/Retriever";
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import "../common.css";

import { setAuthenticationState, setAuthenticationFailure} from  "../reducers/AuthenticationSlice";

const mapDispatch = { 
  setAuthenticationState, 
  setAuthenticationFailure
};

function LoginPage(props) {
  const history = useHistory();
  const [customerId, setCustomerId] = useState("");
  const [password, setPassword] = useState("");

  function validateForm() {
    return customerId.length > 0 && password.length > 0;
  }
  
  function onSuccess(data){
    props.setAuthenticationState(data);
    history.push("/profile/" + data.customerId);
  }

  function onSubmit(event){
    event.preventDefault();
    var login = { customerId, password };
    Retriever.postLogin(login, onSuccess, props.setAuthenticationFailure);
  }

  return (
    <Container className="loginContainer">
      <Form onSubmit={onSubmit}>
        <Form.Group controlId="formGroupEmail">
          <Form.Label>Customer ID</Form.Label>
          <Form.Control 
            type="text" 
            placeholder="Enter Customer ID" 
            value={customerId}
            onChange={e => setCustomerId(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formGroupPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Password" 
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
        </Form.Group>
        <Form.Group>
          <Button type="submit" variant="primary" className="full-width" disabled={!validateForm()}>
          Log In
          </Button>
        </Form.Group>
      </Form>
    </Container>
  );
}

export default connect(null, mapDispatch)(LoginPage);
