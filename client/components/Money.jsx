import React from 'react';

export default function Money({value}){
  return (
    <>
      {value.currency}$ {Math.round((value.amount*100)/100).toFixed(2)}
    </>
  );
}