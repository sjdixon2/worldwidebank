import React, {Component} from 'react';
import { Route, Switch, HashRouter as Router} from 'react-router-dom';
import LoginPage from './components/LoginPage';
import ProfilePage from './components/ProfilePage';
import NotFoundPage from './components/NotFoundPage';
import AuthorizedRoute from './components/AuthorizedRoute';

class App extends Component {
  render(){
    return (
      <Router>
        <Switch>
          <Route exact path='/'>
            <LoginPage />
          </Route>
          <Route exact path='/login'>
            <LoginPage />
          </Route>
          <AuthorizedRoute path='/profile/:id' component={ProfilePage}>
          </AuthorizedRoute>
          <Route>
            <NotFoundPage />
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default App;