import { combineReducers } from 'redux';
import profileDetailsReducer from './ProfileSlice';
import authenticationReducer from './AuthenticationSlice';

export default combineReducers({
  profileDetails: profileDetailsReducer,
  authentication: authenticationReducer
});