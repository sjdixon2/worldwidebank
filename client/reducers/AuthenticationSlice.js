import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  customerId: null,
  token: null
};

const authentication = createSlice({
  name: "authentication",
  initialState,
  reducers: {
    setAuthenticationState(state, action){
      state.customerId = action.payload.customerId;
      state.token = action.payload.token;
    },
    setAuthenticationFailure(state, action){
      state.customerId = null;
      state.token = null;
      state.error = action.payload;
    },
    logout(state){
      state.customerId = null;
      state.token = null;
      state.error = null;
    }
  }
});

export default authentication.reducer;

export const { setAuthenticationState, setAuthenticationFailure, logout} = authentication.actions;