import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  customerId: null,
  name: null,
  accounts: [],
  error: null
};

const profileDetails = createSlice({
  name: "profileDetails",
  initialState,
  reducers: {
    setProfileDetailsSuccess(state, action){
      state.customerId = action.payload.customerId;
      state.name = action.payload.name;
      state.accounts = action.payload.accounts;
    },
    setProfileDetailsFailure(state, action){
      state.customerId = null;
      state.name = null;
      state.accounts = [];
      state.error = action.payload;
    }
  }
});

export default profileDetails.reducer;

export const { setProfileDetailsSuccess, setProfileDetailsFailure} = profileDetails.actions;