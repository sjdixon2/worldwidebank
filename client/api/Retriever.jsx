import axios from 'axios';

class Retriever {
  wrap(axiosCall, onSuccess, onFailure){
    return axiosCall.then(function(response){
      onSuccess(response.data);
    })
    .catch(function(error){
      onFailure(error.response);
    });
  }

  post(url, body, token, onSuccess, onFailure){
    var axiosPost = axios.post(url, body, {
      headers: {
        Authorization: "Bearer " + (token || "")
      }
    });
    return this.wrap(axiosPost, onSuccess, onFailure);
  }

  getCustomer(customerId, token, onSuccess, onFailure){
    console.log(token);
    var axiosCall = axios.get('/api/customers/' + customerId, {
      headers: {
        Authorization: "Bearer " + (token || "")
      }
    });
    return this.wrap(axiosCall, onSuccess, onFailure);
  }

  createDeposit(deposit, token, onSuccess, onFailure){
    console.log(deposit);
    return this.post('/api/deposits', deposit, token, onSuccess, onFailure);
  }

  createWithdrawal(withdrawal, token, onSuccess, onFailure){
    return this.post('/api/withdrawals', withdrawal, token, onSuccess, onFailure);
  }

  createTransfer(transfer, token, onSuccess, onFailure){
    return this.post('/api/transfers', transfer, token, onSuccess, onFailure);
  }

  postLogin(login, onSuccess, onFailure){
    return this.post('api/login', login, "NONE", onSuccess, onFailure);
  }
}

var retriever = new Retriever();

export default retriever;