const path = require('path');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	mode: 'development',
	entry: './client/index.jsx',
	devtool: 'source-map',

	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'dist')
	},

	plugins: [
		new webpack.ProgressPlugin(),
		new HtmlWebpackPlugin({
			template: "./client/index.ejs"
		})
	],

	resolve: {
		extensions: ['*', '.js', '.jsx']
	},

	module: {
		rules: [
			{
				test: /.(js|jsx)$/,
				include: [
					path.resolve(__dirname, 'client')
				],
				loader: 'babel-loader',

				options: {
					plugins: [
						'@babel/syntax-dynamic-import',
						'@babel/plugin-syntax-jsx'
					],

					presets: [
						'@babel/preset-env',
						'@babel/preset-react'
					]
				}
			},

			{
				test: /\.css$/,
				use: [
					'style-loader',
					'css-loader'
				]
			},

      {
				test: /\.ejs$/,
				use: [{
					loader: "ejs-webpack-loader"
				}]
		}
		]
	},

	optimization: {
		splitChunks: {
			cacheGroups: {
				vendors: {
					priority: -10,
					test: /[\\/]node_modules[\\/]/
				}
			},

			chunks: 'async',
			minChunks: 1,
			minSize: 30000,
			name: true
		}
	},

	devServer: {
		open: true
	}
};
