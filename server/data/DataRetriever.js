var _ = require('lodash');
var rawCustomers = require("./customers.json");
var rawAccounts = require("./accounts.json");
var rawTransactions = require("./transactions.json");

var customers = _.keyBy(rawCustomers, customer => customer.customerId);
var accounts = _.keyBy(rawAccounts, account => account.accountId);

var AccountHolder = require('../models/AccountHolder');

class DataRetrievers {
  getAccountById(accountId){
    const account = accounts[accountId];
    return account;
  }

  getAccountsByCustomerId(customerId){
    var customer = this.getCustomerById(customerId);
    var accountIds = customer.accountIds;
    var accounts = _.map(accountIds, this.getAccountById);
    return _.keyBy(accounts, account => account.accountId);
  }

  getAccountHoldersByAccountId(accountId){
    var account = this.getAccountById(accountId);
    var accountHolders = _.map(account.customerIds, customerId => {
      const customer = this.getCustomerById(customerId);
      return new AccountHolder(customer.customerId, customer.name);
    });
    return accountHolders;
  }
  
  getCustomerById(customerId){
    return customers[customerId];
  }
  
  getOutgoingTransactions(accountId){
    var outgoingTransactions = _.groupBy(rawTransactions, transaction => transaction.fromAccountId);
    return outgoingTransactions[accountId];
  }

  getIncomingTransactions(accountId){
    var incomingTransactions = _.groupBy(rawTransactions, transaction => transaction.toAccountId);
    return incomingTransactions[accountId];
  }

  addTransaction(transaction){
    rawTransactions.push(transaction);
  }
}

module.exports = new DataRetrievers();