var idCounter = 100;

class UniqueIdProvider {
  static getUniqueID(){
    return idCounter++;
  }
}

module.exports = UniqueIdProvider;