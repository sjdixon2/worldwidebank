var _ = require("lodash");
var DataRetrievers = require("../data/DataRetriever.js");
var Transaction = require("../models/Transaction");
var AccountDetails = require("../models/AccountDetails");
var Customer = require('../models/Customer');

function getCustomerDetails(customerId){
  var customer = DataRetrievers.getCustomerById(customerId);
  var customerAccounts = DataRetrievers.getAccountsByCustomerId(customerId);
  var customerAccountDetails = _.map(customerAccounts, function(account){
    return getAccountDetails(account.accountId);
  });

  return new Customer(customer.customerId, customer.name, customerAccountDetails);
}

function getAccountDetails(accountId){
  var debits = _.map(DataRetrievers.getIncomingTransactions(accountId), Transaction.newTransaction);
  var credits = _.map(DataRetrievers.getOutgoingTransactions(accountId), Transaction.newTransaction);
  var accountHolders = DataRetrievers.getAccountHoldersByAccountId(accountId);
  return new AccountDetails(accountId, debits, credits, accountHolders);
}

function getAccountByID(accountId){
  return DataRetrievers.getAccountById(accountId);
}

function getAccountsByCustomerId(customerId){
  return DataRetrievers.getAccountsByCustomerId(customerId);
}

function addTransaction(transaction){
  DataRetrievers.addTransaction(transaction);
}

module.exports = {
  getCustomerDetails,
  getAccountDetails,
  getAccountByID,
  addTransaction,
  getAccountsByCustomerId
};