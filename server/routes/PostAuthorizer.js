const HttpStatusCode = require("./HttpStatusCodes.js");
const Authorization = require("../models/Authorization");

module.exports = function(request, response, next){
  console.log("Received authorized request: " + request.method + " " + request.url);
  var customerId = request.body.customerId;
  if (Authorization.isValid(customerId, request)){
    next();
  }
  else {
    response.status(HttpStatusCode.FORBIDDEN).send();
  }
  console.log("Sending response: " + response.statusCode);
};