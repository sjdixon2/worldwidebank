var _ = require("lodash");
var helpers = require("./helpers.js");
var Transaction = require("../models/Transaction");
var HttpStatusCode = require("./HttpStatusCodes.js");

function createTransfer(request, response){
  var toAccountId = request.body.toAccountId;
  var fromAccountId = request.body.fromAccountId;
  var customerId = request.body.customerId;

  if (fromAccountId === toAccountId){
    response.status(HttpStatusCode.BAD_REQUEST).send("Source and destination accounts must be different");
    return;
  }
  if (request.body.value == null || request.body.value <= 0){
    response.status(HttpStatusCode.BAD_REQUEST).send("Requested transfer amount must exist and be greater than zero.");
    return;
  }

  var toAccount = helpers.getAccountByID(toAccountId);
  var fromAccount = helpers.getAccountByID(fromAccountId);

  if (!toAccount){
    response.status(HttpStatusCode.BAD_REQUEST).send("Could not create transfer; no transferee account found.");
    return;
  }
  if (!fromAccount){
    response.status(HttpStatusCode.BAD_REQUEST).send("Could not create transfer; no outgoing account found to transfer from.");
    return;
  }
  
  const transfer = Transaction.newTransfer(customerId, fromAccountId, toAccountId, request.body.value);
  const withdrawalAccount = helpers.getAccountDetails(fromAccountId);
  var requestedTransferValue = transfer.value.getAmountInCanadianDollars();
  var fromAccountBalance = withdrawalAccount.balance.getAmountInCanadianDollars();

  if (fromAccountBalance < requestedTransferValue){
    response.status(HttpStatusCode.PAYMENT_REQUIRED).send("Insufficient balance in account " + fromAccountId + " to create transfer");
    return;
  }

  helpers.addTransaction(transfer);
  response.status(HttpStatusCode.OK).end();
}

module.exports = createTransfer;