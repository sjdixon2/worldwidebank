var express = require("express");
var router = express.Router();

const login = require('./login.js');
const getCustomer = require("./customers.js");
const createTransfer = require("./transfers.js");
const createDeposit = require("./deposits.js");
const createWithdrawal = require("./withdrawals.js");
const PostAuthorizer = require('./PostAuthorizer');

router.get("/", function(request, response){
  response.render("index")
});

router.post("/api/login", login);

router.get("/api/customers/:customerId", getCustomer);

router.use(PostAuthorizer);
router.post("/api/withdrawals", createWithdrawal);
router.post("/api/deposits", createDeposit);
router.post("/api/transfers", createTransfer);

module.exports = router;
