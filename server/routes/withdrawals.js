var _ = require("lodash");
var helpers = require("./helpers.js");
var Transaction = require("../models/Transaction.js");
var HttpStatusCode = require("./HttpStatusCodes.js");

function createWithdrawal(request, response){  
  var accountId = request.body.accountId;
  var customerId = request.body.customerId;
  var account = helpers.getAccountByID(accountId);

  if (!account){
    response.status(HttpStatusCode.BAD_REQUEST).send("Could not find withdrawal account.");
    return;
  }
  if (request.body.value == null || request.body.value.amount <= 0){
    response.status(HttpStatusCode.BAD_REQUEST).send("Requested withdrawal amount must exist and be greater than zero");
    return;
  }

  var customerAccounts = helpers.getAccountsByCustomerId(customerId);
  if (customerAccounts == null || customerAccounts[accountId] == null){
    console.log("Customer " + customerId + " is not a joint account owner on " + accountId);
    response.status(HttpStatusCode.BAD_REQUEST).send("Could not find withdrawal account.");
    return;
  }

  const withdrawalAccount = helpers.getAccountDetails(accountId);
  const withdrawal = Transaction.newWithdrawal(customerId, accountId, request.body.value);


  console.log(request.body);
  var accountBalance = withdrawalAccount.balance.getAmountInCanadianDollars();
  var requestedValue = withdrawal.value.getAmountInCanadianDollars();
  
  if (accountBalance < requestedValue){
    response.status(HttpStatusCode.PAYMENT_REQUIRED).send("Could not create withdrawal - insuffficient balance.");
    return;
  }

  helpers.addTransaction(withdrawal);
  response.status(HttpStatusCode.OK).end();
}

module.exports = createWithdrawal;