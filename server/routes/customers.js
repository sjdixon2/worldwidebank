var helpers = require("./helpers.js");
const Authorization = require("../models/Authorization.js");
const HttpStatusCode = require("./HttpStatusCodes.js");

function getCustomer(request, response){
  var customerId = request.params.customerId;
  if (!Authorization.isValid(customerId, request)){
    response.status(HttpStatusCode.FORBIDDEN).send();
  }
  else {
    var customer = helpers.getCustomerDetails(customerId);
    response.send(customer);
  }
}

module.exports = getCustomer;