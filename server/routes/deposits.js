var _ = require("lodash");
var helpers = require("./helpers.js");
var Transaction = require("../models/Transaction");
var HttpStatusCode = require("./HttpStatusCodes");

function createDeposit(request, response){
  var accountId = request.body.accountId;
  var customerId = request.body.customerId;
  var depositAccount = helpers.getAccountByID(accountId);

  if (!depositAccount){
    response.status(HttpStatusCode.BAD_REQUEST).send("Could not find deposit account.");
    return;
  }

  if (request.body.value == null || request.body.value.amount <= 0){
    response.status(HttpStatusCode.BAD_REQUEST).send("Amount must be greater than zero");
    return;
  }

  var customerAccounts = helpers.getAccountsByCustomerId(customerId);
  if (customerAccounts == null || customerAccounts[accountId] == null){
    console.log("Customer " + customerId + " is not a joint account owner on " + accountId);
    response.status(HttpStatusCode.BAD_REQUEST).send("Could not find deposit account.");
    return;
  }
  
  const deposit = Transaction.newDeposit(customerId, accountId, request.body.value);

  helpers.addTransaction(deposit);
  response.status(HttpStatusCode.OK).end();
}

module.exports = createDeposit;