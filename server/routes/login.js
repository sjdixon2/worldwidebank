const HttpStatusCode = require("./HttpStatusCodes");
var Authorization = require('../models/Authorization');

function login(request, response){
  if (request.body.password == "admin"){
    var customerId = request.body.customerId;
    var authorized = Authorization.newAuthorizationToken(customerId);
    response.status(HttpStatusCode.OK).send(authorized);
  } else {
    var notAuthorized = Authorization.loginFailure();
    response.status(HttpStatusCode.UNAUTHORIZED).send(notAuthorized);
  }
}

module.exports = login;