const Currency = Object.freeze({
  CAD: "CAD",
  USD: "USD",
  MXN: "MXN"
});

module.exports = Currency;