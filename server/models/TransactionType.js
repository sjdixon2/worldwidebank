const AccountActivity = require("./Transaction");

const TransactionType = Object.freeze({
  WITHDRAWAL: "WITHDRAWAL",
  DEPOSIT: "DEPOSIT",
  TRANSFER: "TRANSFER"
});

module.exports = TransactionType;