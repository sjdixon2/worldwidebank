const Money = require('./Money');
const Currency = require('./Currency');

test('getAmountInCanadianDollars on Canadian money return raw amount as a primitive number', () => {
  const value = new Money(123.0, Currency.CAD);
  expect(value.getAmountInCanadianDollars()).toEqual(123.0);
  expect(value.convertToCanadianDollars()).toEqual(value);
});

test('getAmountInCanadianDollars on American money should return twice the amount', () => {
  const value = new Money(123.0, Currency.USD);
  expect(value.getAmountInCanadianDollars()).toEqual(246.0);
  expect(value.convertToCanadianDollars()).toEqual(new Money(246.0, Currency.CAD));
});

test('getAmountInCanadianDollars on MXN money should return one tenth the amount', () => {
  const value = new Money(100.0, Currency.MXN);
  expect(value.getAmountInCanadianDollars()).toEqual(10.0);
  expect(value.convertToCanadianDollars()).toEqual(new Money(10.0, Currency.CAD));
});

test('constructor should parse int and string values', () => {
  const valueWithString = new Money("100.0", Currency.MXN);
  const valueWithNumber = new Money(100.0, Currency.MXN);
  expect(valueWithNumber).toEqual(valueWithString);
})