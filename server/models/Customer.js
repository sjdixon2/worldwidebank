var AccountHolder = require('./AccountHolder');

class Customer extends AccountHolder {
  constructor(customerId, name, accounts){
    super(customerId, name);
    this.accounts = accounts;
  }
}

module.exports = Customer;