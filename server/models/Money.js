const Currency = require("./Currency.js");

class Money {
  constructor(amount, currency){
    this.amount = parseInt(amount);
    this.currency = currency;
  }

  getAmountInCanadianDollars(){
    if (this.currency == Currency.CAD){
      return this.amount;
    } else if (this.currency == Currency.USD){
      return this.amount * 2;
    } else if (this.currency == Currency.MXN){
      return this.amount / 10;
    } else {
      throw new Error("Cannot convert " + this.currency);
    }
  }

  convertToCanadianDollars(){
    var amountInCanadianDollars = this.getAmountInCanadianDollars();
    return new Money(amountInCanadianDollars, Currency.CAD);
  }
}

module.exports = Money;