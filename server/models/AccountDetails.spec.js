var AccountDetails = require('./AccountDetails');
const AccountHolder = require('./AccountHolder');
const Currency = require('./Currency');
const Money = require('./Money');
const Transaction = require('./Transaction');

const ACCOUNT_ID = "432";
const CUSTOMER_ID = "4123";
const CUSTOMER_NAME = "Harry Styles";
const ACCOUNT_HOLDER = new AccountHolder(CUSTOMER_ID, CUSTOMER_NAME);
const DEPOSIT = Transaction.newDeposit(CUSTOMER_ID, ACCOUNT_ID, new Money(250.0, Currency.MXN));
const WITHDRAWAL = Transaction.newWithdrawal(CUSTOMER_ID, ACCOUNT_ID, new Money(7, Currency.USD));
const EXPECTED_BALANCE = new Money(250/10 - (7*2), Currency.CAD);

test('Constructor should accept basic arguments', () => {
  const account = new AccountDetails(ACCOUNT_ID, [], [], []);
  expect(account.accountId).toBe(ACCOUNT_ID);
  expect(account.debits).toHaveLength(0);
  expect(account.credits).toHaveLength(0);
  expect(account.accountHolders).toHaveLength(0);
  expect(account.balance).toBeTruthy();
  expect(account.balance.amount).toEqual(0);
  expect(account.balance.currency).toEqual(Currency.CAD);
});

test('Constructor can accept array arguments', () => {
  const account = new AccountDetails(ACCOUNT_ID, [DEPOSIT], [WITHDRAWAL], [ACCOUNT_HOLDER]);
  expect(account.accountId).toBe(ACCOUNT_ID);
  expect(account.accountHolders).toHaveLength(1);
  expect(account.debits).toHaveLength(1);
  expect(account.credits).toHaveLength(1);

  expect(account.accountHolders).toContain(ACCOUNT_HOLDER);
  expect(account.debits).toContain(DEPOSIT);
  expect(account.credits).toContain(WITHDRAWAL);
  expect(account.balance).toStrictEqual(EXPECTED_BALANCE);
})