var _ = require("lodash");
const Money = require("./Money");
const Currency = require("./Currency");

class AccountDetails {
  constructor(accountId, debits, credits, accountHolders){
    this.accountId = accountId;
    this.debits = debits;
    this.credits = credits;
    this.accountHolders = accountHolders;
    this.balance = this.getAccountBalanceInCanadianDollars();
  }

  getAccountBalanceInCanadianDollars(){
    var totalDepositsAndIncomingTransfers = _.sumBy(this.debits, this.getActivityAmountInCommonCurrency);
    var totalWithdrawalsAndOutgoingTransfers = _.sumBy(this.credits, this.getActivityAmountInCommonCurrency);
    var difference = totalDepositsAndIncomingTransfers - totalWithdrawalsAndOutgoingTransfers;
    if (difference === undefined){
      difference = 0;
    }
    return new Money(difference, Currency.CAD);
  }

  getActivityAmountInCommonCurrency(activity){
    var money = activity.getAmount();
    return money.getAmountInCanadianDollars();
  }
}

module.exports = AccountDetails;