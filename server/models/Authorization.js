class Authorization {
  constructor(customerId, token, error){
    this.customerId = customerId;
    this.token = token;
    this.error = error;
  }

  static newAuthorizationToken(customerId){
    return new Authorization(customerId, "VALID"+ customerId, null);
  }

  static loginFailure(){
    return new Authorization(null, null, "Login failed");
  }

  static isValid(customerId, request){
    var expected = "Bearer VALID"+customerId;
    var token = request.get("Authorization");
    console.log(expected, token);
    return token === expected;
  }
}

module.exports = Authorization;