const TransactionType = require("./TransactionType.js");
const Currency = require('./Currency.js');
const Money = require("./Money.js");
const UniqueIdProvider = require('../data/UniqueIdProvider');

function getUniqueID(){
  return UniqueIdProvider.getUniqueID();
}

class Transaction {
  constructor(transactionID, customerId, transactionType,  fromAccountId, toAccountId, value){
    this.transactionID = transactionID;
    this.customerId = customerId;
    this.transactionType = transactionType;
    this.fromAccountId = fromAccountId;
    this.toAccountId = toAccountId;
    this.value = value;
  }

  getAmount(){
    return this.value;
  }

  static newTransfer(customerId, fromAccountId, toAccountId, requestedValue){
    var transactionID = getUniqueID();
    var transferValue = new Money(requestedValue.amount, Currency[requestedValue.currency]);
    return new Transaction(transactionID, customerId, TransactionType.TRANSFER, fromAccountId, toAccountId, transferValue);
  }

  static newDeposit(customerId, accountId, requestedValue){
    var transactionID = getUniqueID();
    var depositValue = new Money(requestedValue.amount, Currency[requestedValue.currency]);
    return new Transaction(transactionID, customerId, TransactionType.DEPOSIT, null, accountId, depositValue);
  }

  static newWithdrawal(customerId, accountId, requestedValue){
    var transactionID = getUniqueID();
    var withdrawalValue = new Money(requestedValue.amount, Currency[requestedValue.currency]);
    return new Transaction(transactionID, customerId, TransactionType.WITHDRAWAL, accountId, null, withdrawalValue);
  }

  static newTransaction(rawTransaction){
    var value = new Money(rawTransaction.value.amount, rawTransaction.value.currency);
    return new Transaction(rawTransaction.transactionID, rawTransaction.customerId, rawTransaction.transactionType, rawTransaction.fromAccountId, rawTransaction.toAccountId, value);
  }

}

module.exports = Transaction;