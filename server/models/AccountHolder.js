class AccountHolder {
  constructor(customerId, name){
    this.customerId = customerId;
    this.name = name;
  }
}

module.exports = AccountHolder;