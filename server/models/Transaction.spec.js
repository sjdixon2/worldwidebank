const UniqueIdProvider = require('../data/UniqueIdProvider');
const Transaction = require('./Transaction');
const Money = require('./Money');
const Currency = require('./Currency');

jest.mock('../data/UniqueIdProvider');

const TRANSACTION_ID = 431;
const ACCOUNT_ID = 432;
const CUSTOMER_ID = 4123;
const CUSTOMER_NAME = "Harry Styles";
const TRANSACTION_VALUE = new Money(25.0, Currency.CAD);


test('New deposit is created with unique ID', () => {
  const mockGetUniqueID = jest.fn();
  mockGetUniqueID.mockReturnValue(99);
  UniqueIdProvider.getUniqueID = mockGetUniqueID;
  
  const deposit = Transaction.newDeposit(CUSTOMER_ID, ACCOUNT_ID, TRANSACTION_VALUE);
  expect(deposit.transactionID).toEqual(99);
  expect(deposit.fromAccountId).toBeNull();
  expect(deposit.toAccountId).toBe(ACCOUNT_ID);
  expect(deposit.value).toEqual(TRANSACTION_VALUE);
  expect(deposit.getAmount()).toEqual(TRANSACTION_VALUE);
});