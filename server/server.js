var express = require("express");
var router = require("./routes/routes.js")
var path = require("path"); 
var HttpStatusCodes = require('./routes/HttpStatusCodes');

var app = express();
var dist = path.join(__dirname, "../dist");
var assets = path.join(__dirname, "../assets");

app.set("view engine", "ejs");
app.set("views", dist);
app.use(express.static(dist));
app.use("/assets", express.static(assets));
app.use(express.json());

app.use(router);



module.exports = app;
